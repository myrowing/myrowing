# use bash
SHELL=/usr/bin/bash

TRGDIR=/opt/myrowing

# generate random passwords
$(eval DB_ROOT_PASSWORD = $(shell pwgen -scn 20 1))
$(eval DB_WRITE_PASSWORD = $(shell pwgen -scn 20 1))
$(eval DB_READ_PASSWORD = $(shell pwgen -scn 20 1))
$(eval GF_ADMIN_PASSWORD = $(shell pwgen -scn 20 1))

# replace placeholders with generated passwords in template files if the target files do not exist yet
all:
	@if ! [ -f "$(DESTDIR)$(TRGDIR)/.env" ]; then \
		sed -e "s/<DB_ROOT_PASSWORD>/$(DB_ROOT_PASSWORD)/; s/<DB_WRITE_PASSWORD>/$(DB_WRITE_PASSWORD)/; s/<DB_READ_PASSWORD>/$(DB_READ_PASSWORD)/; s/<GF_ADMIN_PASSWORD>/$(GF_ADMIN_PASSWORD)/" < ./env.1 > ./.env; \
	fi
	@if ! [ -f "$(DESTDIR)$(TRGDIR)/init.sql/01.sql" ]; then \
		sed -e "s/<DB_READ_PASSWORD>/$(DB_READ_PASSWORD)/" < ./01.sql.1 > ./01.sql; \
	fi	

.PHONY: all install

# create folder structure and copy files to destinations
install:
	install -dm755 $(DESTDIR)$(TRGDIR)
	install -dm755 $(DESTDIR)$(TRGDIR)/datamgmt
	install -dm777 $(DESTDIR)$(TRGDIR)/grafana
	install -dm777 $(DESTDIR)$(TRGDIR)/log

	@if [ -f ./.env ]; then \
		install -Dm600 ./.env $(DESTDIR)$(TRGDIR)/.env; \
	fi	
	@if [ -f ./01.sql ]; then \
		install -Dm644 ./01.sql $(DESTDIR)$(TRGDIR)/init.sql/01.sql; \
	fi	
	install -Dm600 ./docker-compose.yml $(DESTDIR)$(TRGDIR)/docker-compose.yml

# remove intermediate data
clean:
	rm -f ./01.sql ./.env
